<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Example Site</title>

    <!-- This is Typekit. I set up the fonts needed and it should ensure they work for you -->
    <link rel="stylesheet" href="https://use.typekit.net/xkq5qzz.css" />
    <!-- This is BootStrap Grid  -->
    <link rel="stylesheet" href="assets/styles/libs/bootstrap-grid.min.css" />
    <!-- This is your stylesheet file  -->
    <link rel="stylesheet" href="assets/styles/main.css" />
</head>
<body>
    <?php 
        // Some of these includes use Bootstrap Grid classes.
        // For more info on Bootstrap, go to https://getbootstrap.com/docs/4.0/layout/grid/

        include('components/head.php'); 
        include('components/banner.php'); 
        include('components/module-image-text.php');

        // The module below is the same as the one above, but it will display with the image first and the text second
        // 
        $reverse = true;
        include('components/module-image-text.php');

        include('components/module-quote.php');

        // I've not included everything here, just a few bits to illustrate how it would work.

        include('components/foot.php'); 
    ?>

    <!-- This is your main js file  -->
    <script src="assets/scripts/main.js"></script>
</body>