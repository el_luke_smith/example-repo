/* This is where any custom Javascript goes */

/* Mobile Menu */

var scripts = {
    init: function() {
        this.mobileMenu();
    },
    mobileMenu: function() {
        this.nav = document.querySelector('.nav');
        var trigger = document.querySelector('.nav-trigger');
        if ( trigger ) trigger.addEventListener('click', this.menuToggle.bind(this));
    },
    menuToggle: function() {
        if (this.nav.classList.contains('nav--open')) {
            this.nav.classList.remove('nav--open');
        } else {
            this.nav.classList.add('nav--open');
        }
    }
}

scripts.init();

