<div class="banner">
    <!-- This would contain the content for the slider. I've done a single slide as an example -->
    <div class="banner__slide" style="background-image: url(http://placekitten.com/800/280)">
        <div class="container">
            <!-- The image for the slider will be uploaded to the CMS, which means it will be dynamically added to the slider via PHP. I've done an inline banner above to show how tha would be applied -->
            <div class="banner__content">
                <!-- this is in a container, as you'll likely need to add the white overlay as a background image. This one can be applied in the style sheet, not as an inline style -->
                <h1 class="banner__heading heading1">Heading 1</h1>
                <div class="banner__copy">
                    <!-- You could put this ina paragraph if it's the same as the paragraph copy - I haven't as I think it might be slightly different -->
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </div>
            </div>
        </div>
    </div>
</div>