<!-- The container class limits the width, so it doesn't always go to the edges of the screen -->
<div class="module module-image-text container">
    <!-- The PHP code in the line below checks to see if a $reverse value is set to true. If it is true, it will add a class
        to the row called 'flex-row-reverse'. This class will cause the order of the elements in the row to be in reverse.
        eg - Instead of 'Text - Image' it will render as 'Image - Text'. -->
    <div class="row <?php if ($reverse == true): ?>flex-row-reverse<?php endif; ?>">
        <div class="module-image-text__text col-lg">
            <h2 class="heading2">Heading 2</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris malesuada nec ipsum in molestie. Etiam dictum mi elementum eros ullamcorper, ut lobortis tellus euismod. Curabitur nec urna eu enim malesuada porttitor id porta tellus. Nullam eget metus sed sapien cursus maximus quis eget nulla. Pellentesque ac scelerisque nulla. Quisque dignissim erat non blandit vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vitae nibh sit amet neque ultricies bibendum. Nam sed venenatis lorem.</p>
        </div>
        <div class="module-image-text__image col-lg" style="background-image: url(http://placekitten.com/500/300);">
            <!-- You might make this a background image, just incase you want the image to scale or change -->
        </div>
    </div>
</div>