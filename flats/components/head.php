<header class="header container">
    <div class="row">
        <div class="logo">
            <img class="logo__image" src="https://via.placeholder.com/200x80.png?text=Logo" />
        </div>
        <div class="nav-trigger ml-auto">X</div>
        <nav class="nav ml-auto collapse">
            <div class="nav-trigger ml-auto">X</div>
            <ul class="nav__list">
                <li class="nav__item">
                    <a class="nav__link" href="#">Home</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link" href="#">Program</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link" href="#">Case Studies</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link" href="#">About Us</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link" href="#">Get Involved</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link" href="#">Contact</a>
                </li>
            </ul>
        </nav>
    </div>
</header>