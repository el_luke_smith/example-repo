<?php

    function assetVersion($path, $return_path=true) {
        $full_path = get_template_directory() . $path; 
        if ($return_path === false) return filemtime( $full_path ); 
        else return get_template_directory_uri() . $path . '?v=' . filemtime( $full_path ); 
    }