<?php

    /*
        Remove Comments
    */

    // Removes comments from admin menu
    add_action( 'admin_menu', function() {
        remove_menu_page( 'edit-comments.php' );
    });

    // Removes comments from post and pages
    add_action('init', function() {
        remove_post_type_support( 'post', 'comments' );
        remove_post_type_support( 'page', 'comments' );
    }, 100);

    // Removes from admin bar
    add_action( 'wp_before_admin_bar_render', function() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('comments');
    });


    /*
        Remove Default Posts
    */

    // Removes default post type from admin menu
    add_action( 'admin_menu', function() {
        remove_menu_page( 'edit.php' );
    });

    // Removes default post type from admin bar
    add_action( 'admin_bar_menu', function( $wp_admin_bar ) {
        $wp_admin_bar->remove_node( 'new-post' );
    }, 999 );

    // Removes default post type from dashboard
    add_action( 'wp_dashboard_setup', function() {
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    }, 999 );