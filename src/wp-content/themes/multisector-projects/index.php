<?php 

    get_header();

    // Some of these includes use Bootstrap Grid classes.
    // For more info on Bootstrap, go to https://getbootstrap.com/docs/4.0/layout/grid/

    include('components/header.php'); 

    get_template_part('components/banner'); 
    get_template_part('components/page-builder');

    include('components/footer.php'); 

    get_footer();
?>
