<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo get_bloginfo('name'); ?></title>

    <?php wp_head(); ?>

    <!-- This is Typekit. I set up the fonts needed and it should ensure they work for you -->
    <link rel="stylesheet" href="https://use.typekit.net/xkq5qzz.css" />
</head>
<body>