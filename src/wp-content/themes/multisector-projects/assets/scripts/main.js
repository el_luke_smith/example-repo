/* This is where any custom Javascript goes */

/* Mobile Menu */

var scripts = {
    
    init: function() {
        this.mobileMenu();
        this.setupCrousels();
    },

    mobileMenu: function() {
        this.nav = document.querySelector('.nav');
        var trigger = document.querySelector('.nav__trigger');
        if ( trigger ) trigger.addEventListener('click', this.menuToggle.bind(this));
    },

    menuToggle: function() {
        if (this.nav.classList.contains('nav--open')) {
            this.nav.classList.remove('nav--open');
        } else {
            this.nav.classList.add('nav--open');
        }
    },

    setupCrousels() {

        // Home banner
        tns({
            container: '.banner--slider',
            controls: false,
            items: 1,
            slideBy: 'page',
            // nextButton: ".next-button",
            // prevButton: ".prev-button",
            nav: true,
            autoplayButtonOutput: false,
        });
    }
}

scripts.init();

