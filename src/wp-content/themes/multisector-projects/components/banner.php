<div class="banner">
    <!-- This would contain the content for the slider. I've done a single slide as an example -->
    <?php if( have_rows('banner_slider') ): ?>
        <div class="banner--slider">
            <?php while ( have_rows('banner_slider') ): the_row(); ?>
                <?php $image = get_sub_field('image'); ?>
                <div class="banner__slide">
                    <div class="banner__background" style="background-image: url(<?php echo $image['sizes']['large']; ?>)">
                        <div class="container">
                            <div class="banner__content row">
                                <!-- this is in a container, as you'll likely need to add the white overlay as a background image. This one can be applied in the style sheet, not as an inline style -->
                                <h1 class="banner__heading heading1"><?php the_sub_field('heading'); ?></h1>
                                <div class="banner__copy">
                                    <?php the_sub_field('content'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
</div>