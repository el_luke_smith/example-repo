<!-- The container class limits the width, so it doesn't always go to the edges of the screen -->
<div class="module module-content-and-image container">
    <!-- The PHP code in the line below checks to see if a sub field called 'order' is set to 'left'. If it is true, it will 
        add a class to the row called 'flex-row-reverse'. This class will cause the order of the elements in the row to be 
        in reverse. eg - Instead of 'Text - Image' it will render as 'Image - Text'. -->
    <div class="row <?php if (get_sub_field('order') == 'left'): ?>flex-row-reverse<?php endif; ?>">
        <div class="module-content-and-image__text col-lg">
            <h2 class="heading2"><?php the_sub_field('heading'); ?></h2>
            <?php the_sub_field('content'); ?>
        </div>
        <?php $image = get_sub_field('image'); ?>
        <div class="module-content-and-image__image col-lg" style="background-image: url(<?php echo $image['sizes']['medium_large']; ?>);"></div>
    </div>
</div>