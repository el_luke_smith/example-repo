<!-- The container class limits the width, so it doesn't always go to the edges of the screen -->
<div class="module module-quote">
    <div class="container">
        <div class="row justify-content-center">
            <div class="module-quote__text col-md-12 col-lg-10 heading2">
                <?php the_sub_field('quote'); ?>
            </div>
        </div>
    </div>
</div>