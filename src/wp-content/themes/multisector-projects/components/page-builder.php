<?php

    if ( have_rows('page_builder_group') ) {

        // loop through the rows of data
        while ( have_rows('page_builder_group') ) {
            the_row();

            if ( get_row_layout() ) {
                get_template_part( 'components/module-' . str_replace('_', '-', get_row_layout()) );
            }
    
        }
    
    }